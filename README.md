# Technical Test DevOps - Instituto Atlântico
### criado por Everton Maia
<br>

### Ferramentas
- Terraform
- Jenkins
- Ansible
- Gitlab

### Nuvem
- AWS

### Aplicação e Banco de Dados
- NodeJS 8
- MySQL 5.7


## Etapas
1. Provisionar infraestrutura(EC2, VPC Custom) na AWS com o Terraform;
2. Instalação das ferramentas: Docker, Jenkins na instancia dev-ops(t2.micro);
3. Instalação da ferramenta Docker e deploy da aplicação na instancia dev-deploy(t2.micro) utilizando o ansible;


### Provisionamento da Infraestrutura

1. Fazer o build do Jenkins:
```
cd jenkins
docker build -t evertondocker/jenkins-lab:latest .
docke push evertondocker/jenkins-lab:latest
```

2. Alterar o arquivo docker-compose.yml no mesmo diretório com a tag da imagem acima que será utilizado pelo script de inicialização da EC2 que subirá com o Jenkins dentro do diretório terraform/entry-script-dev-ops.sh

```
File: docker-compose.yml

  jenkins:
    container_name: jenkins
    image: evertondocker/jenkins-lab:latest
```
3. Dentro do script 'entry-script-dev-ops.sh' tem os comandos para instalar o docker, git, docker-compose e subir o jenkins em container, utilizando o docker-compose baixado no gitlab.

```
File: entry-script-dev-ops.sh

#!/bin/bash
yum update -y &&  yum install -y docker
systemctl start docker
usermod -aG docker ec2-user
chkconfig docker on
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
curl https://gitlab.com/evertonmaia/ia-technical-test-devops/-/raw/master/jenkins/docker-compose.yml --output docker-compose.yaml
docker-compose up -d

```
4. O script 'entry-script-dev-deploy.sh', prepara a instância ec2 de deploy para receber os containers buildados pelo jenkins e provisionados pelo ansible.
```
File: entry-script-dev-deploy.sh

#!/bin/bash
yum update
yum install -y yum-utils
yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io -y
systemctl start docker
systemctl enable docker
usermod -a -G docker ec2-user
yum install -y python36

```
5. Editar e preencher as variáveis necessárias no arquivo terraform.auto-sample-tfvars depois renomeá-lo para terraform.auto.tfvars.

```
File: terraform.auto-sample-tfvars

vpc_cidr_blocks    = "" #10.0.0.0/16
subnet_cidr_blocks = "" #10.0.10.0/24
avail_zone         = "" #us-east-1c
env_prefix         = "" #dev/prod/test
aws_access_key     = "" #your access key id from AWS account
aws_secret_key     = "" #your secret key from AWS account
instance_type      = "" #t2.micro   type of shape instance
ssh_public_key     = "" #pass your public ssh key 
```
6. Validar o código, gerar o arquivo de plan e por último executa-lo com apply.

```
cd terraform
terraform init
terraform validate
terraform plan --out=ia-lab-agosto2021
terraform apply ia-lab-agosto2021

```

### Ao final da execução do comando terraform apply, o plano irá exibir as informações abaixo, variando os valores de acordo com a infraestrutura que foi provisionada em sua conta na aws.

```
ami_image_id = "ami-0c2b8ca1dad447f8a"
private_ip_deploy_app = "10.0.10.125"
private_ip_jenkins = "10.0.10.87"
public_ip_instance_deploy_app = "54.196.232.199"
public_ip_instance_jenkins = "54.197.2.63"
```
Jenkins: http://54.197.2.63:8080 <br>
Aplicação: http://54.196.232.199:4000/

## Acessar a console web do Jenkins e fazer as configurações: <br>
- Credenciais do Dockerhub para realização do PUSH, com o nome de DockerHubEverton. ( Configurado no Jenkinsfile )
- Adicionar uma crendencial do tipo 'ssh username and private key' para que o ansible possa fazer a comunicação com a ec2 de deploy;
nome da chave: dev-server ( Configurado no Jenkinsfile )
user: ec2-user
<br>
### Alterar o arquivo dentro da pasta ansible/inventory.inv e colocar o ip interno da maquina deploy_app, no caso no meu lab o ip correto é:

```
private_ip_deploy_app = "10.0.10.125"
```
<br>

### Criar um novo Item no Jenkins, do tipo "PIPELINE", apontando para o repositório no Gitlab https://gitlab.com/evertonmaia/ia-technical-test-devops.git, onde está arquivo Jenkinsfile com a configuração do Pipeline. ( Não é ncessário configurar credenciais, visto que o repositório está público)

## O arquivo Jenkinsfile fará:

- Build do Frontend em NodeJS
- Build do Banco de Dados no serviço Mysql (Que chamo de Backend nos scripts)
- Push para o DockerHub das duas imagens (Frontend e Backend)
- Deploy através do playbook do ansible que se conecta na ec2 de deploy e realiza a entrega das imagens e execução dos containers.

```
[root@ip-10-0-10-125 ec2-user]# docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED          STATUS          PORTS                               NAMES
f263fe4f2977   evertondocker/frontend:dev   "docker-entrypoint.s…"   13 minutes ago   Up 13 minutes   0.0.0.0:4000->4000/tcp              frontend
7635deacb54f   evertondocker/backend:dev    "docker-entrypoint.s…"   14 minutes ago   Up 14 minutes   33060/tcp, 0.0.0.0:6603->3306/tcp   backend
[root@ip-10-0-10-125 ec2-user]# 
```

![Jenkins Pipeline](./images/jenkins.png)


### Logs Ansible no Jenkins

```
[Pipeline] { (Deploy to Dev)
[Pipeline] ansiblePlaybook
[ia-tec] $ ansible-playbook /var/jenkins_home/workspace/ia-tec/ansible/docker-deployment.yml -i /var/jenkins_home/workspace/ia-tec/ansible/inventory.inv --private-key /var/jenkins_home/workspace/ia-tec/ssh14976265794137104112.key -u ec2-user -e TAG=dev -e ENV=dev1

PLAY [dev1] ********************************************************************

TASK [Gathering Facts] *********************************************************
ok: [10.0.10.125]

TASK [install python dependencies] *********************************************
ok: [10.0.10.125]

TASK [pip] *********************************************************************
ok: [10.0.10.125]

TASK [Remover serviço Backend] *************************************************
changed: [10.0.10.125]

TASK [Iniciar serviço Backend] *************************************************
changed: [10.0.10.125]

TASK [Remover serviço Frontend] ************************************************
changed: [10.0.10.125]

TASK [Remove image] ************************************************************
changed: [10.0.10.125]

TASK [Iniciar serviço Frontend] ************************************************

changed: [10.0.10.125]

PLAY RECAP *********************************************************************
10.0.10.125                : ok=8    changed=5    unreachable=0    failed=0   

[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```

![AWS](./images/aws.png)

## Cadastrar Alunos na Base de Dados através do método POST.
```
curl --header "Content-Type: application/json" -d '{"rollNo": 3483501, "name": "Everton Maia"}' -X POST 54.196.232.199:4000/add-student
```
## Consultar Alunos na Base de Dados.
```
curl -X POST http://54.196.232.199:4000/get-students
```
