#!/bin/bash
yum update -y &&  yum install -y docker
systemctl start docker
usermod -aG docker ec2-user
chkconfig docker on
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
curl https://gitlab.com/evertonmaia/ia-technical-test-devops/-/raw/master/jenkins/docker-compose.yml --output docker-compose.yaml
docker-compose up -d
